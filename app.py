from flask import Flask, request, jsonify
from elasticsearch import Elasticsearch
import csv
from flask_sqlalchemy import SQLAlchemy
from config import Config
import datetime


app = Flask(__name__)
app.config.from_object(Config)
app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']])
db = SQLAlchemy(app)
from models import Docs
db.create_all()


@app.route("/start/")
def start():
    Docs.query.delete()
    app.elasticsearch.indices.delete(index='test-index', ignore=[400, 404])
    with open('posts.csv', newline='', encoding="utf-8") as csvfile:
        reader = csv.reader(csvfile)
        reader.__next__()
        i = 0
        for row in reader:
            i += 1
            row.append(i)
            db.session.add(Docs(text=row[0], created_date=datetime.datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S'), rubrics=row[2], id=row[3]))
            app.elasticsearch.index(index="test-index", id=i, body={'text': row[0]})
    db.session.commit()
    return 'Данные сохранены!'


@app.route("/search/")
def search():
    text = request.args.get('text')
    result = []
    dict = []
    d = app.elasticsearch.search(index="test-index", size=20, body={"query": {"match": {'text': text}}}, ignore=404)
    if 'hits' in d:
        for _ in d['hits']['hits']:
            result.append(_['_id'])
        p = Docs.query.filter(Docs.id.in_(result)).order_by(Docs.created_date).all()
        for j in p:
            dict.append({'id': j.id, 'rubrics': j.rubrics, 'text': j.text, 'date': j.created_date})
    return jsonify(dict)


@app.route("/delete/")
def delete():
    ids = request.args.get('id')
    p = Docs.query.filter_by(id=ids)
    for k in p:
        db.session.delete(k)
    db.session.commit()
    app.elasticsearch.delete(index="test-index", id=ids, ignore=404)
    return 'Данные удалены!'


if __name__ == '__main__':
    app.run()
