from app import db


class Docs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rubrics = db.Column(db.String)
    text = db.Column(db.String)
    created_date = db.Column(db.DateTime)